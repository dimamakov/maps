package crud.app.city.retrofit;

import crud.app.city.model.Company;
import retrofit.Callback;
import retrofit.RestAdapter;
import retrofit.http.GET;
import retrofit.http.Query;

/**
 * Created by Dmitriy Makovetskiy on 29.09.2017.
 * dmitriy.makovetskiyua@gmail.com
 */

public class Retrofit {

    private static final String ENDPOINT = "http://lovecity.net.ua";
    private static ApiInterface apiInterface;

    static {
        initialize();
    }

    interface ApiInterface {
        @GET("/api/cities/10/companies")
        void getCompanies(Callback<Company> callback);
    }

    private static void initialize () {
        RestAdapter restAdapter = new RestAdapter.Builder()
                .setEndpoint(ENDPOINT)
                .setLogLevel(RestAdapter.LogLevel.FULL)
                .build();
        apiInterface = restAdapter.create(ApiInterface.class);
    }

    public static void getCompanies (Callback<Company> callback) {
        apiInterface.getCompanies(callback);
    }

}
