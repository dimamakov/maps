package crud.app.city.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import crud.app.city.R;
import crud.app.city.model.MyCompany;

/**
 * Created by Dmitriy Makovetskiy on 29.09.2017.
 * dmitriy.makovetskiyua@gmail.com
 */

public class CompanyAdapter extends RecyclerView.Adapter<CompanyAdapter.ViewHolder> {

    private Context context;
    private List<MyCompany> companies;

    public CompanyAdapter(Context context, List<MyCompany> companies) {
        this.context = context;
        this.companies = companies;
    }

    @Override
    public CompanyAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View item = LayoutInflater.from(context).inflate(R.layout.item_company, parent, false);
        return new CompanyAdapter.ViewHolder(item);
    }

    @Override
    public void onBindViewHolder(final CompanyAdapter.ViewHolder holder, int position) {

        // init field
        holder.name.setText(companies.get(position).getName());
        holder.click(context, companies.get(position).getId().toString());
    }

    @Override
    public int getItemCount() {
        return companies.size();
    }

    static class ViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.company_logo)
        ImageView logo;

        @BindView(R.id.company_name)
        TextView name;

        @BindView(R.id.toast_layout)
        FrameLayout toastLL;

        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }

        public void click(final Context context, final String id) {
            toastLL.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Toast.makeText(context, id, Toast.LENGTH_SHORT).show();
                }
            });
        }
    }
}