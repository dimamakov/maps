package crud.app.city.activities;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import crud.app.city.R;

public class ListActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_list);
    }
}
