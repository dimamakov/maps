package crud.app.city.activities;

import android.app.DatePickerDialog;
import android.content.Context;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.Gravity;
import android.view.View;
import android.widget.DatePicker;
import android.widget.LinearLayout;
import android.widget.TabHost;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import crud.app.city.R;
import crud.app.city.adapter.CompanyAdapter;
import crud.app.city.model.Company;
import crud.app.city.model.MyCompany;
import crud.app.city.retrofit.Retrofit;
import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

public class MainActivity extends AppCompatActivity implements DatePickerDialog.OnDateSetListener,
        OnMapReadyCallback, GoogleMap.OnMarkerClickListener {

    @BindView(R.id.recycler_view)
    RecyclerView recyclerView;
    @BindView(R.id.tabHost)
    TabHost tabHost;
    @BindView(R.id.ll_main)
    LinearLayout ll;

    Calendar calendar = Calendar.getInstance();
    List<MyCompany> names = new ArrayList<>();
    Company mCompany = new Company();
    SupportMapFragment smp;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);

        getData();
        setTabHost();

        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        recyclerView.setAdapter(new CompanyAdapter(this, names));
    }

    private void getData() {
        Retrofit.getCompanies(new Callback<Company>() {
            @Override
            public void success(Company company, Response response) {

                mCompany = company;

                createMapView(MainActivity.this);

                for (int i = 0; i < company.getData().size(); i++) {
                    names.add(new MyCompany(company.getData().get(i).getName(),
                            company.getData().get(i).getId()));
                }

                Toast.makeText(getApplicationContext(), "Downloaded", Toast.LENGTH_SHORT).show();
            }

            @Override
            public void failure(RetrofitError error) {
                Toast.makeText(getApplicationContext(), "Error", Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void setTabHost() {
        tabHost.setup();
        tabHost.addTab(tabHost.newTabSpec("tag1")
                .setIndicator("Map")
                .setContent(R.id.act_map));
        tabHost.addTab(tabHost.newTabSpec("tag2")
                .setIndicator("List")
                .setContent(R.id.act_list));
    }

    private void createMapView(Context context) {

        smp = (SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.map);
        smp.getMapAsync((OnMapReadyCallback) context);

    }

    @OnClick(R.id.fab)
    void onClick() {
        new DatePickerDialog(MainActivity.this, this,
                calendar.get(Calendar.YEAR),
                calendar.get(Calendar.MONTH),
                calendar.get(Calendar.DAY_OF_MONTH))
                .show();
    }

    @Override
    public void onDateSet(DatePicker datePicker, int i, int i1, int i2) {
        Toast.makeText(this, i + " - " + i1 + " - " + i2, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {

        for (int i = 0; i < mCompany.getData().size(); i++) {
            MarkerOptions mo = new MarkerOptions();
            mo.position(new LatLng(mCompany.getData().get(i).getLatitude(), mCompany.getData().get(i).getLongitude()))
                    .icon(BitmapDescriptorFactory.fromResource(R.mipmap.ic_launcher))
                    .title(mCompany.getData().get(i).getName());
            googleMap.addMarker(mo);
        }

        googleMap.setOnMarkerClickListener(this);
    }

    @Override
    public boolean onMarkerClick(Marker marker) {

        Snackbar snackB = Snackbar.make(ll, marker.getTitle(), Snackbar.LENGTH_LONG);

        View snackBarView = snackB.getView();

        TextView snackBText = snackBarView.findViewById(android.support.design.R.id.snackbar_text);
        snackBText.setCompoundDrawablesWithIntrinsicBounds(android.R.mipmap.sym_def_app_icon, 0, 0, 0);
        snackBText.setGravity(Gravity.CENTER);
        snackB.show();

        return true;
    }
}
