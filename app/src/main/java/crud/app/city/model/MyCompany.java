package crud.app.city.model;

/**
 * Created by Dmitriy Makovetskiy on 29.09.2017.
 * dmitriy.makovetskiyua@gmail.com
 */

public class MyCompany {

    public MyCompany() {
    }

    public MyCompany(String name, Integer id) {
        this.name = name;
        this.id = id;
    }

    private String name;
    private Integer id;

    public String getName() {
        return name;
    }

    public Integer getId() {
        return id;
    }
}
