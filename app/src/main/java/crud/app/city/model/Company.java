package crud.app.city.model;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Dmitriy Makovetskiy on 29.09.2017.
 * dmitriy.makovetskiyua@gmail.com
 */

public class Company {

    private Integer code;
    private String message;
    private List<Datum> data = new ArrayList<>();

    public static class Datum {
        private Integer id;
        private Integer owner_id;
        private Integer category_id;
        private Object trc_id;
        private Integer city_id;
        private String name;
        private String subcategory;
        private String address;
        private Double longitude;
        private Double latitude;
        private String phone;
        private String site;
        private String description;
        private String discount;
        private Integer is_active;
        private String keywords;
        private String type;
        private String updated_at;
        private String created_at;
        private Integer has_share;
        private List<WorkTime> work_time = new ArrayList<>();

        public String getName() {
            return name;
        }

        public Integer getId() {
            return id;
        }

        public Double getLongitude() {
            return longitude;
        }

        public Double getLatitude() {

            return latitude;
        }

        public static class WorkTime {

            private String name;
            private String alias;
            private Integer company_id;
            private String from;
            private String to;
            private Integer is_day_off;

        }
    }

    public List<Datum> getData() {
        return data;
    }
}
